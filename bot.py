import markovify
import discord
import asyncio
import random

client = discord.Client()
bot_token = "NTA3MjI2MTY1OTA4NjAyODgx.Dr3rwQ.r2lKaKuO4KUqqbPJ7LdF28VSAC8"
text_model = None
with open("./data/evil_james.txt") as f:
    print("Reading corpus.")
    text = f.read()
    text_model = markovify.Text(text)
    print("Text model ready.")

@client.event
async def on_ready():
    print("Connected to server.")


@client.event
async def on_message(message):
    print("Got a message!")
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    random_number = random.randint(1,100)
    print(f'Random number was {random_number}')

    if client.user in message.mentions or random_number <= 10:

        reply = text_model.make_sentence()
        await client.send_message(message.channel, reply)

client.run(bot_token)
